jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"sap/ui/sta/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment"
], function (BaseController, MessageToast, Fragment) {
	"use strict";
	var thes;
	return BaseController.extend("sap.ui.sta.controller.Home", {

		onInit: function (oEvent) {
			thes = this;

			var url = "/bacrcr001-srv/bacrcr001/Requisitions?$expand=review";

			$.ajax({
				url: url,
				success: function (result) {
					var jsonModel = new sap.ui.model.json.JSONModel(result.value);
					thes.byId("tabla").setModel(jsonModel);
				},
				error: function (err) {

				}
			});
		},

		onItemPress: function (oEvent) {
			var reg = oEvent.getParameters().item.getBindingContext().getProperty();
			this.getRouter().navTo("detalle", {
				ID: reg.review_ID,
				ID_REQUISITIONS: reg.ID
			});
		}

	});
});