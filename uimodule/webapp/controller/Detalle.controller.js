jQuery.sap.require("sap.m.MessageBox");
sap.ui.define([
	"sap/ui/sta/controller/BaseController",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/ui/unified/FileUploader"
], function (BaseController, MessageToast, Fragment, FileUploader) {
	"use strict";
	var Z_OD_SCP_BASRRC001_TAB_SRV;
	var Z_OD_SCP_BASRRC001_ORD_SRV;
	var thes;
	var werks;
	return BaseController.extend("sap.ui.sta.controller.Detalle", {

		onInit: function () {

			var oRouter = this.getRouter();
			oRouter.getRoute("detalle").attachMatched(this._onRouteMatched, this);

		},

		_onRouteMatched: function (oEvent) {
			thes = this;

			var oArgs = oEvent.getParameter("arguments");
			thes.byId("page").destroyContent();

			var url = "/bacrcr001-srv/bacrcr001/Reviews(" + oArgs.ID + ")?$expand=sections($expand=questions($expand=answers,requestAnswers))";

			$.ajax({
				url: url,
				async: false,
				success: function (result) {
					var iconTabBar = new sap.m.IconTabBar();

					if (result.sections.length > 0) {

						result.sections = result.sections.sort(function (a, b) {
							var x = a.title.toLowerCase();
							var y = b.title.toLowerCase();
							if (x < y) {
								return -1;
							}
							if (x > y) {
								return 1;
							}
							return 0;
						});

						for (var a = 0; a < result.sections.length; a++) {

							var iconTabFilter = new sap.m.IconTabFilter({
								text: result.sections[a].title
							});
							var simpleForm = new sap.ui.layout.form.SimpleForm({
								editable: true,
								layout: "ResponsiveLayout",
								adjustLabelSpan: true,
								singleContainerFullSize: true,
								width: "100%"
							});

							result.sections[a].questions = result.sections[a].questions.sort(function (ab, ba) {
								var x = ab.title.toLowerCase();
								var y = ba.title.toLowerCase();
								if (x < y) {
									return -1;
								}
								if (x > y) {
									return 1;
								}
								return 0;
							});

							for (var e = 0; e < result.sections[a].questions.length; e++) {

								if (result.sections[a].questions[e].questionType) {

									var label = new sap.m.Label({
										text: result.sections[a].questions[e].title
									});

									simpleForm.addContent(label);

									switch (result.sections[a].questions[e].questionType) {
										case "BOOLEAN":
											if (result.sections[a].questions[e].requestAnswers) {
												var oCtrl04 = new sap.m.ComboBox({
													items: [
														new sap.ui.core.Item({
															text: "SI",
															key: "SI"
														}), new sap.ui.core.Item({
															text: "NO",
															key: "NO"
														})
													]
												});
												oCtrl04.data("id", result.sections[a].questions[e].requestAnswers.ID);
												oCtrl04.setSelectedKey(result.sections[a].questions[e].requestAnswers.value);
												simpleForm.addContent(oCtrl04);
											}

											break;
										case "ARCHIVO":
											if (result.sections[a].questions[e].requestAnswers) {
												var oCtrl06 = new FileUploader();
												oCtrl06.data("id", result.sections[a].questions[e].requestAnswers.ID);
												oCtrl06.setValue(result.sections[a].questions[e].requestAnswers.value);
												simpleForm.addContent(oCtrl06);
											}
											break;
										case "TEXTO":
											if (result.sections[a].questions[e].requestAnswers) {
												var oCtrl07 = new sap.m.Input({
													placeholder: result.sections[a].questions[e].title
												});
												oCtrl07.data("id", result.sections[a].questions[e].requestAnswers.ID);
												oCtrl07.setValue(result.sections[a].questions[e].requestAnswers.value);
												simpleForm.addContent(oCtrl07);
											}

											break;

										case "SINGLE":
											if (result.sections[a].questions[e].requestAnswers) {

												result.sections[a].questions[e].answers = result.sections[a].questions[e].answers.sort(function (a, b) {
													var x = a.value.toLowerCase();
													var y = b.value.toLowerCase();
													if (y < x) {
														return -1;
													}
													if (y > x) {
														return 1;
													}
													return 0;
												});

												for (var i = 0; i < result.sections[a].questions[e].answers.length; i++) {
													var oCtrl05 = new sap.m.RadioButton({
														text: (result.sections[a].questions[e].answers[i].value),
														groupName: (result.sections[a].questions[e].answers[i].question_ID)
													});
													oCtrl05.data("id", result.sections[a].questions[e].requestAnswers.ID);
													if (result.sections[a].questions[e].requestAnswers.value === result.sections[a].questions[e].answers[i].value) {
														oCtrl05.setSelected(true);
													}
													simpleForm.addContent(oCtrl05);
												}
											}
											break;
										case "MULTIPLE":
											if (result.sections[a].questions[e].requestAnswers) {
												for (var i = 0; i < result.sections[a].questions[e].answers.length; i++) {
													var oCtrl08 = new sap.m.CheckBox({
														text: (result.sections[a].questions[e].answers[i].value),
														wrapping: true
													});
													oCtrl08.data("id", result.sections[a].questions[e].requestAnswers.ID);
													if (result.sections[a].questions[e].requestAnswers.value.length) {
														var valorMultiple = JSON.parse(result.sections[a].questions[e].requestAnswers.value);
														for (var o = 0; o < valorMultiple.length; o++) {
															if (valorMultiple[o] === result.sections[a].questions[e].answers[i].value) {
																oCtrl08.setSelected(true);
															}
														}
														simpleForm.addContent(oCtrl08);
													}
												}
											}
											break;
									}

									// for (var i = 0; i < result.sections[a].questions[e].answers.length; i++) {
									// 	if (result.sections[a].questions[e].questionType) {

									// 		switch (result.sections[a].questions[e].questionType.title) {
									// 		case "SELECCIÓN SIMPLE":
									// 			var oCtrl05 = new sap.m.RadioButton({
									// 				text: (result.sections[a].questions[e].answers[i].textAnswer),
									// 				groupName: (result.sections[a].questions[e].answers[i].question_ID)
									// 			});
									// 			simpleForm.addContent(oCtrl05);
									// 			break;
									// 		case "SELECCIÓN MÚLTIPLE":
									// 			var oCtrl08 = new sap.m.CheckBox({
									// 				text: (result.sections[a].questions[e].answers[i].textAnswer),
									// 				wrapping: true
									// 			});
									// 			simpleForm.addContent(oCtrl08);
									// 			break;
									// 		}
									// 	}
									// }

								}

							}
							iconTabFilter.addContent(simpleForm);
							iconTabBar.addItem(iconTabFilter);
						}

						var url2 = "/bacrcr001-srv/bacrcr001/Requisitions(" + oArgs.ID_REQUISITIONS + ")?$expand=review";

						$.ajax({
							url: url2,
							async: false,
							success: function (result) {

								var objectHeader = new sap.m.ObjectHeader({
									title: result.review.title,
									intro: result.review.description,
									responsive: true
								});

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Asignado",
									text: result.reviewer_ID
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Sociedad",
									text: result.review.company + " - " + result.review.companyDesc
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Centro",
									text: result.review.plant + " - " + result.review.plantDesc
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Marca",
									text: result.brand + " - " + result.brandDesc
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Modelo",
									text: result.model + " - " + result.modelDesc
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Año",
									text: result.year
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Color",
									text: result.colorExtDesc
								});
								objectHeader.addAttribute(objectAttribute);

								// var objectAttribute = new sap.m.ObjectAttribute({
								// 	title: "Segmento",
								// 	text: result.review.segment + " - " + result.review.segmentDesc
								// });
								// objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Gama",
									text: result.review.gama
								});
								objectHeader.addAttribute(objectAttribute);

								var objectAttribute = new sap.m.ObjectAttribute({
									title: "Estado",
									text: result.status
								});
								objectHeader.addAttribute(objectAttribute);

								thes.byId("page").addContent(objectHeader);
							},
							error: function (err) {

							}
						});

						thes.byId("page").addContent(iconTabBar);
					}

				},
				error: function (err) {

				}
			});

		},

		onGuardar: function () {

			var contentSimpleForm = this.byId("page").getContent()[0].mAggregations._header.oSelectedItem.getContent()[0].getContent();

			var requestAnswers = [];
			var valueCheckBox = [];
			var checkBoxId = [];

			// var json = {
			// 	imagen: "",
			// 	booleano: "",
			// 	abierta: "",
			// 	simple: "",
			// 	multiple: []
			// };

			for (var a = 0; a < contentSimpleForm.length; a++) {
				switch (contentSimpleForm[a].getMetadata()._sClassName) {
					case "sap.m.Label":
						break;
					case "sap.m.ComboBox":
						var json = {
							ID: "",
							value: ""
						};
						json.value = contentSimpleForm[a].getValue();
						json.ID = contentSimpleForm[a].data("id");
						requestAnswers.push(json);
						break;
					case "sap.ui.unified.FileUploader":
						var json = {
							ID: "",
							value: ""
						};
						json.value = contentSimpleForm[a].getValue();
						json.ID = contentSimpleForm[a].data("id");
						requestAnswers.push(json);
						break;
					case "sap.m.CheckBox":
						var json = {
							ID: "",
							value: ""
						};
						if (contentSimpleForm[a].getSelected()) {
							checkBoxId = contentSimpleForm[a].data("id")
							valueCheckBox.push(contentSimpleForm[a].getText());
						}
						break;
					case "sap.m.RadioButton":
						var json = {
							ID: "",
							value: ""
						};
						if (contentSimpleForm[a].getSelected()) {
							json.value = contentSimpleForm[a].getText();
							json.ID = contentSimpleForm[a].data("id");
							requestAnswers.push(json);
						}
						break;
					case "sap.m.Input":
						var json = {
							ID: "",
							value: ""
						};
						json.value = contentSimpleForm[a].getValue();
						json.ID = contentSimpleForm[a].data("id");
						requestAnswers.push(json);
						break;
				}
			}

			if (valueCheckBox.length > 0) {
				var json = {
					ID: "",
					value: ""
				};
				json.value = JSON.stringify(valueCheckBox);
				json.ID = checkBoxId;
				requestAnswers.push(json);
			}

			for (var a = 0; a < requestAnswers.length; a++) {

				var url = "/bacrcr001-srv/bacrcr001/RequestAnswers(" + requestAnswers[a].ID + ")";

				var oError = false;

				$.ajax({
					type: "PUT",
					async: false,
					data: JSON.stringify(requestAnswers[a]),
					url: url,
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					success: function (result) {

					},
					error: function (err) {
						thes.oError = true;
					}
				});

			}

			if (thes.oError) {
				sap.m.MessageToast.show("Error al guardar");
			} else {
				sap.m.MessageToast.show("Se ha guardo con éxito!");
			}

			//sap.m.MessageToast.show(JSON.stringify(requestAnswers));

		},

		mostrarMensajes: function (mensajes) {

			/*var mensajes = [{
				"Type": "E",
				"Message": "Mensaje de prueba"
			}];*/

			for (var i = 0; i < mensajes.length; i++) {
				switch (mensajes[i].Type) {
					case "E":
						mensajes[i].Type = "Error";
						break;
					case "S":
						mensajes[i].Type = "Success";
						break;
					case "W":
						mensajes[i].Type = "Warning";
						break;
					case "I":
						mensajes[i].Type = "Information";
						break;
					case "error":
						mensajes[i].Type = "Error";
						break;
					case "success":
						mensajes[i].Type = "Success";
						break;
					case "warning":
						mensajes[i].Type = "Warning";
						break;
					case "information":
						mensajes[i].Type = "Information";
						break;
				}
			}

			var oMessageTemplate = new sap.m.MessageItem({
				type: '{Type}',
				title: '{Message}'
			});

			var oModel = new sap.ui.model.json.JSONModel(mensajes);

			var oBackButton = new sap.m.Button({
				icon: sap.ui.core.IconPool.getIconURI("nav-back"),
				visible: false,
				press: function () {
					thes.oMessageView.navigateBack();
					this.setVisible(false);
				}
			});

			thes.oMessageView = new sap.m.MessageView({
				showDetailsPageHeader: false,
				itemSelect: function () {
					oBackButton.setVisible(true);
				},
				items: {
					path: "/",
					template: oMessageTemplate
				}
			});

			thes.oMessageView.setModel(oModel);

			thes.oDialog = new sap.m.Dialog({
				resizable: true,
				content: thes.oMessageView,
				beginButton: new sap.m.Button({
					press: function () {
						thes.oDialog.close();
					},
					text: "Close"
				}),
				customHeader: new sap.m.Bar({
					contentMiddle: [
						new sap.m.Text({
							text: "Mensajes"
						})
					],
					contentLeft: [oBackButton]
				}),
				contentHeight: "50%",
				contentWidth: "50%",
				verticalScrolling: false
			});

			thes.oMessageView.navigateBack();
			thes.oDialog.open();

		}

	});
});