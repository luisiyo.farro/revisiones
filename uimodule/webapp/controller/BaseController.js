sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function (Controller, History) {
	"use strict";

	return Controller.extend("sap.ui.sta.controller.BaseController", {

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		onNavBack: function (oEvent) {
			var oHistory, sPreviousHash;

			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("home", {}, true /*no history*/ );
			}
		},

		dateToString: function (value) {
			var dia = (value.getDate() + 1).toString();
			if (dia.length === 1) {
				dia = "0" + dia;
			}
			var mes = (value.getMonth() + 1).toString();
			if (mes.length === 1) {
				mes = "0" + mes;
			}
			var year = (value.getFullYear()).toString();

			return year + mes + dia;

		},

		onCerrarPopUp: function (oEvent) {
			var id = oEvent.getSource().data("id");
			this.byId(id).close();
		}

	});

});